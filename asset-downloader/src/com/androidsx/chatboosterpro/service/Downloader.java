package com.androidsx.chatboosterpro.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


import android.util.Log;

/**
 * Downloads a file from the internet.
 */
class Downloader {
    private static final String TAG = Downloader.class.getSimpleName();
    
    /**
     * Downloads the file in the provided URL into the provided file.
     * <p>
     * It notifies the user about the progress, but it does NOT notify about the success or failure of the operation.
     * 
     * @return true if and only if the download succeeded
     */
    boolean download(String urlToDownload, File file, ServiceProgressNotifier progressNotifier) {
        try {
            final URL url = new URL(urlToDownload);
            final URLConnection connection = url.openConnection();
            connection.connect();
            downloadInternal(url, file, connection.getContentLength(), progressNotifier);
            return true;
        } catch (IOException e) {
            Log.e(TAG, "Can't download " + urlToDownload + " into " + file, e);
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Can't download " + urlToDownload + " into " + file, e);
            return false;
        }
    }

    private void downloadInternal(URL url, File file, long fileLength, ServiceProgressNotifier progressNotifier) throws IOException {
        final InputStream input = new BufferedInputStream(url.openStream(), 8192);
        final OutputStream output = new FileOutputStream(file);
        try {
            final byte data[] = new byte[8192];
            long total = 0;
            int count;
            double lastPercentageReported = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
                final double percentage = (double) total * 100.0 / fileLength;
                if (percentage > lastPercentageReported + 5) { // Notify only every 5%
                    progressNotifier.onProgress(percentage);
                    lastPercentageReported = percentage;
                }
            }
            progressNotifier.onProgress(100);
        } finally {
            output.flush();
            output.close();
            input.close();
        }
    }

}
