package com.androidsx.chatboosterpro.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.jakewharton.notificationcompat2.NotificationCompat2;
import com.jotabout.zipdownloader.R;

/**
 * Keeps the notification bar updated with the progress.
 * <p>
 * On devices running 2.x, it does not show the progress bar, but only the text :(
 */
class ServiceProgressNotifier {
    private static final String TAG = ServiceProgressNotifier.class.getSimpleName();
    private static final int ONGOING_NOTIFICATION_ID = 100;

    private final Context context;
    private final NotificationManager mNotificationManager;
    private final NotificationCompat2.Builder builder;
    
    ServiceProgressNotifier(Context context) {
        this.context = context;
        this.mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        this.builder = createOngoingNotificationBuilder();
    }
    
    void onStart() {
        Log.d(TAG, "Start installing the HD images");
        final Notification ongoingNotification = builder
                .setProgress(100, 0, false)
                .build();
        ongoingNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(ONGOING_NOTIFICATION_ID, ongoingNotification);
    }
    
    void onProgress(double percentage) {
        Log.d(TAG, "Progress: " + percentage + "%");
        final Notification ongoingNotification = builder
                .setNumber((int) percentage)
                .setProgress(100, (int) percentage, false)
                .setContentText(createContentText((int) percentage))
                .build();
        ongoingNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(ONGOING_NOTIFICATION_ID, ongoingNotification);
    }
    
    void onCustomProgress(String message) {
        Log.d(TAG, "Custom progress: " + message);
        final Notification ongoingNotification = builder
                .setNumber(100)
                .setProgress(100, 100, false)
                .setContentText(message)
                .build();
        ongoingNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(ONGOING_NOTIFICATION_ID, ongoingNotification);
    }
    
    void onEnd() {
        Log.i(TAG, "Done intalling the HD images");
        
        final Notification ongoingNotification = builder
                .setOngoing(false)
                .setContentText(context.getString(R.string.toast_hd_downloaded_success)) // TODO: strings.xml
                .build();
        ongoingNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(ONGOING_NOTIFICATION_ID, ongoingNotification);
    }
    
    // TODO: Provide meaningful message for the user
    void onFailure() {
        final Notification ongoingNotification = builder
                .setOngoing(false)
                .setContentText(context.getString(R.string.toast_hd_images_failed)) // TODO: strings.xml
                .build();
        ongoingNotification.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(ONGOING_NOTIFICATION_ID, ongoingNotification);
    }
    
    private String createContentText(int percentage) {
        return context.getString(R.string.notification_hd_in_progress) + percentage + "%";
    }

    private NotificationCompat2.Builder createOngoingNotificationBuilder() {
        final Intent resultIntent = new Intent();
        final PendingIntent contentIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
        
        final NotificationCompat2.Builder mBuilder = new NotificationCompat2.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setOngoing(true)
                .setContentIntent(contentIntent)
                .setContentText(createContentText(0));
        
        return mBuilder;
    }
}
