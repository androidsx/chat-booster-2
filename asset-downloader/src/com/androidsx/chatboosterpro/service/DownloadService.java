package com.androidsx.chatboosterpro.service;

import java.io.File;
import java.io.IOException;

import com.jotabout.zipdownloader.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;


/**
 * Service that downloads a file from an internet URL and then extracts it. It notifies the user via bar notifications.
 * <p>
 * See {@link #onHandleIntent} to learn what extras to pass in.
 * <p>
 * Note that this service will perform the download and unzip even if it was already done.
 */
public class DownloadService extends IntentService {
    private static final String TAG = DownloadService.class.getSimpleName();
    
    public DownloadService() {
        super(TAG);
    }

    /**
     * Expected extras:
     * <ul>
     * <li>url (String): the internet URL where the file to be downloaded is</li>
     * <li>unzippedFolder (String): the full path for the directory where we'll unzip it all</li>
     * </ul>
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        final String urlToDownload = intent.getStringExtra("url");
        final String unzippedFolder = intent.getStringExtra("unzippedFolder");
        
        final ServiceProgressNotifier progressNotifier = new ServiceProgressNotifier(this);
        File zipFile = null;
        try {
            zipFile = createNewTempFile(); // typically /mnt/sdcard/Android/data/com.androidsx.chatboosterpro/tmp/hd.zip
            progressNotifier.onStart();
            final Downloader downloader = new Downloader();
            Log.i(TAG, "Download the HD images from " + urlToDownload + " into " + zipFile.getAbsolutePath());
            final boolean downloaded = downloader.download(urlToDownload, zipFile, progressNotifier);
            
            boolean unzipped = false;
            if (downloaded) {
                Log.i(TAG, "Unzip the HD images from " + zipFile.getAbsolutePath() + " into " + unzippedFolder);
                progressNotifier.onCustomProgress(getString(R.string.notification_hd_unzipping)); // FIXME: obviously a hack
                final Unzipper unzipper = new Unzipper(zipFile.getAbsolutePath(), unzippedFolder);
                unzipped = unzipper.unzip();
            }
            
            if (downloaded && unzipped) {
                progressNotifier.onEnd();
            } else {
                progressNotifier.onFailure();
            }
        } catch (Exception e) {
            Log.e(TAG, "Unexpected exception while downloading and unzipping", e);
            progressNotifier.onFailure();
        } finally {
            if (zipFile != null) {
                zipFile.delete();
            }
        }
    }
    
    private File createNewTempFile() throws IOException {
        final String baseDirName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/"
                + this.getPackageName();
        final String tmpDirName = baseDirName + "/tmp";
        final File tmpDir = new File(tmpDirName);
        if (!tmpDir.exists()) {
            tmpDir.mkdirs();
        }
        final File file = new File(tmpDir, "hd.zip");
        file.createNewFile();
        return file;
    }
}
