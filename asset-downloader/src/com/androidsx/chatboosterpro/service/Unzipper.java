package com.androidsx.chatboosterpro.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.util.Log;

/**
 * Unzips a ZIP file into a folder. It may contain subdirectories.
 */
class Unzipper {
    private static final String TAG = Unzipper.class.getSimpleName();
    private static final int BUFFER_SIZE = 8192;

    private final String zipFile;
    private final String unzippedFolder;

    /**
     * @param zipFile absolute location of the ZIP file, such as
     *            /mnt/sdcard/Android/data/com.jotabout.zipdownloader/tmp/hd.zip
     * @param unzippedFold erbsolute location inside the sdcard, such as
     *            /mnt/sdcard/Android/data/com.jotabout.zipdownloader/files/unzipped
     */
    Unzipper(String zipFile, String unzippedFolder) {
        this.zipFile = zipFile;
        this.unzippedFolder = unzippedFolder;
    }

    boolean unzip() {
        try {
            unzipInternal();
            return true;
        } catch (IOException e) {
            Log.e(TAG, "Can't unzip " + zipFile + " into " + unzippedFolder, e);
            return false;
        } catch (Exception e) {
            Log.e(TAG, "Can't unzip " + zipFile + " into " + unzippedFolder, e);
            return false;
        }
    }

    private void unzipInternal() throws IOException {
        final byte[] buffer = new byte[BUFFER_SIZE];

        final ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
        try {
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                final String outputRelFileName = ze.getName(); // such as Rage/499__001_lol_guy_original.jpg
                final File outputFile = new File(unzippedFolder + "/" + outputRelFileName);
                if (outputFile.isFile()) {
                    //Log.d(TAG, "Skip the unzipping of the file " + outputRelFileName + ": already exists"); // Too much output
                } else if (outputFile.isDirectory()) {
                    Log.d(TAG, "Skip the unzipping of the directory " + outputRelFileName);
                } else {
                    //Log.d(TAG, "Unzip " + outputRelFileName + " into " + unzippedFolder); // Too much output
                    outputFile.getParentFile().mkdirs();
                    final FileOutputStream fout = new FileOutputStream(outputFile);
                    int count;
                    while ((count = zis.read(buffer)) != -1) {
                        fout.write(buffer, 0, count);
                    }
                    fout.close();
                    zis.closeEntry();
                }
            }
        } finally {
            zis.close();
        }
    }
}
