package com.jotabout.zipdownloader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.androidsx.chatboosterpro.service.DownloadService;
import com.jotabout.zipdownloader.util.ExternalStorage;

/**
 * Example application to try out the {@link DownloadService}.
 * <p>
 * Change the <em>android.library</em> flag to false to try it out.
 */
public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "\n\nNEW VERSION\n\n");
		setContentView(R.layout.main);
	}

	/**
     * Invoked when user presses "Start download" button. Called from the view.
     */
	public void startDownload(View v) {
	    final String url = ((EditText) findViewById(R.id.url_field)).getText().toString();
	    // unzippedFolder example: /mnt/sdcard/Android/data/com.jotabout.zipdownloader/files/unzipped
	    final String unzippedFolder = ExternalStorage.getSDCacheDir(MainActivity.this, "unzipped").getPath();
		startDownloadService(url, unzippedFolder);
	}
	
	/**
	 * Keep in sync with {@code GridActivity}, in chatbooster-pro.
	 */
	private void startDownloadService(String url, String unzippedFolder) {
        final Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("url", url);
        intent.putExtra("unzippedFolder", unzippedFolder);
        Log.i(TAG, "Starting service to download " + url + " into " + unzippedFolder);
        startService(intent);
	}
	
	/**
	 * Loads the URL of a small file in the edit text. Called from the view.
	 */
	public void loadSmallFileUrl(View v) {
	    ((EditText) findViewById(R.id.url_field)).setText("http://androidsx.com/projects/chattoolkit/tmp/small_archive.zip");
	}

    /**
     * Loads the URL of a medium-sized file in the edit text. Called from the view.
     */
    public void loadMediumFileUrl(View v) {
        ((EditText) findViewById(R.id.url_field)).setText("http://androidsx.com/projects/chattoolkit/tmp/medium_archive.zip");
    }

    /**
     * Loads the URL of a big file in the edit text. Called from the view.
     */
    public void loadBigFileUrl(View v) {
        ((EditText) findViewById(R.id.url_field)).setText("http://androidsx.com/projects/chattoolkit/tmp/big_archive.zip");
    }
}
